<?php

namespace offgamers\metronic\bundles;

use Yii;

/**
 * Main backend application asset bundle.
 */
class MetronicAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@metronic_asset';

    public $buildPath = '@build_path';

    public function init()
    {
        Yii::setAlias('@metronic_asset', Yii::$app->metronic->sourcePath);
        Yii::setAlias('@build_path', Yii::$app->metronic->buildPath);

        $this->css = [
            'vendors/base/vendors.bundle.css',
            Yii::getAlias($this->buildPath) . '/base/style.bundle.css',
        ];

        $this->js = [
            'vendors/base/vendors.bundle.js',
            Yii::getAlias($this->buildPath) . '/base/scripts.bundle.js'
        ];
    }
}

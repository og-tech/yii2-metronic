<?php

namespace offgamers\metronic\bundles;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class GlyphiconFixAsset extends AssetBundle
{
    public $sourcePath = '@offgamers/metronic/assets';

    public $css = [
        'css/glyphicon_fix.css',
    ];
    public $js = [
    ];
    public $depends = [
    ];
}

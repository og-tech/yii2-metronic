<?php

namespace offgamers\metronic;

use Yii;
/**
 * This is the class of Metronic Component
 */
class Metronic extends \yii\base\Component
{
    public $buildPath, $sourcePath;

    public function registerCoreAssets($view){
        return bundles\MetronicAsset::register($view);
    }

}
<?php

namespace offgamers\metronic\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Class Menu
 * Theme menu widget.
 */
class Menu extends \yii\widgets\Menu
{
    /**
     * @inheritdoc
     */

    public $linkTemplate = '<a href="{url}" class="m-menu__link "> {icon}  {label} </a>';

    public $labelTemplate = '<span class="m-menu__link-text">{label}</span>';

    public $submenuTemplate = ' <div class="m-menu__submenu"> <span class="m-menu__arrow"></span> <ul class="m-menu__subnav"> {items}</ul> </div>';

    public $multiLayerLabelTemplate = ' <a href="{url}" class="m-menu__link m-menu__toggle"> {icon} {label}<i class="m-menu__ver-arrow la la-angle-right"></i> </a>';

    public $multiLayerLinkTemplate = ' <a href="{url}" class="m-menu__link m-menu__toggle"> {icon} {label}<i class="m-menu__ver-arrow la la-angle-right"></i> </a>';

    public $iconHtml = '<i class="m-menu__link-icon {icon}"></i>';

    public $activateParents = true;

    public $defaultIconHtml = '';

    public $itemOptions = ["class" => "m-menu__item"];

    public $activeCssClass = ' m-menu__item--open m-menu__item--expanded';

    public $subItemCssClass = ' m-menu__item  m-menu__item--submenu';

    public $options = ["class" => "m-menu__nav  m-menu__nav--dropdown-submenu-arrow", "data-menu-vertical" => "true", "data-menu-scrollable" => "false", "data-menu-dropdown-timeout" => "500"];

    /**
     * @var string is prefix that will be added to $item['icon'] if it exist.
     * By default uses for Font Awesome (http://fontawesome.io/)
     */
    public static $iconClassPrefix = 'fa fa-';

    private $noDefaultAction;
    private $noDefaultRoute;

    /**
     * Renders the menu.
     */
    public function run()
    {
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        $posDefaultAction = strpos($this->route, Yii::$app->controller->defaultAction);
        if ($posDefaultAction) {
            $this->noDefaultAction = rtrim(substr($this->route, 0, $posDefaultAction), '/');
        } else {
            $this->noDefaultAction = false;
        }
        $posDefaultRoute = strpos($this->route, Yii::$app->controller->module->defaultRoute);
        if ($posDefaultRoute) {
            $this->noDefaultRoute = rtrim(substr($this->route, 0, $posDefaultRoute), '/');
        } else {
            $this->noDefaultRoute = false;
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');
            echo Html::tag($tag, $this->renderItems($items), $options);
        }
    }

    /**
     * @inheritdoc
     */
    protected function renderItem($item)
    {
        if (isset($item['items'])) {
            $labelTemplate = $this->multiLayerLabelTemplate;
            $linkTemplate = $this->multiLayerLinkTemplate;
        } else {
            $labelTemplate = $this->labelTemplate;
            $linkTemplate = $this->linkTemplate;
        }
        $replacements = [
            '{label}' => strtr($this->labelTemplate, ['{label}' => $item['label'],]),
            '{icon}' => empty($item['icon']) ? $this->defaultIconHtml : strtr($this->iconHtml, ['{icon}' => $item['icon']]),
            '{url}' => isset($item['url']) ? Url::to($item['url']) : 'javascript:void(0);',
        ];

        $template = ArrayHelper::getValue($item, 'template', isset($item['url']) ? $linkTemplate : $labelTemplate);

        return strtr($template, $replacements);
    }

    /**
     * Recursively renders the menu items (without the container tag).
     * @param array $items the menu items to be rendered recursively
     * @return string the rendering result
     */
    protected function renderItems($items)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {
            $options = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'options', []));
            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];
            if ($item['active']) {
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }
            $menu = $this->renderItem($item);
            if (!empty($item['items'])) {
                $menu .= strtr($this->submenuTemplate, [
                    '{show}' => $item['active'] ? $this->activeCssClass : '',
                    '{items}' => $this->renderItems($item['items']),
                ]);
                if (isset($options['class'])) {
                    $options['class'] = $this->subItemCssClass. ' ' . $options['class'];
                } else {
                    $options['class'] = $this->subItemCssClass;
                }
            }
            $lines[] = Html::tag($tag, $menu, $options);
        }
        return implode("\n", $lines);
    }
}

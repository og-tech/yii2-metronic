<?php

namespace offgamers\metronic\widgets;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $separator = '<li class="m-nav__separator">-</li>';

    public $activeItemTemplate = '<li class="m-nav__item">{link}</li>';

    public $linkClass = 'm-nav__link';

    public $labelTemplate = '';

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (empty($this->links)) {
            return;
        }
        $links = [];
        if ($this->homeLink === null) {
            $links[] = '<li class="m-nav__item m-nav__item--home"><a href="/" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a></li>';
        } elseif ($this->homeLink !== false) {
            $links[] = $this->renderItem($this->homeLink, $this->itemTemplate);
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }
        echo Html::tag($this->tag, implode($this->separator, $links), $this->options);
    }

    /**
     * Renders a single breadcrumb item.
     * @param array $link the link to be rendered. It must contain the "label" element. The "url" element is optional.
     * @param string $template the template to be used to rendered the link. The token "{link}" will be replaced by the link.
     * @return string the rendering result
     * @throws InvalidConfigException if `$link` does not have "label" element.
     */
    protected function renderItem($link, $template)
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? Html::encode($link['label']) : $link['label'];

            if($this->labelTemplate){
                $label = strtr($this->labelTemplate, ['{label}' => $label]);
            }

        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }
        if (isset($link['url'])) {
            $options = $link;
            $class = $this->linkClass;
            unset($options['template'], $options['label'], $options['url']);
            $link = Html::a($label, $link['url'], ['class' => $class]);
        } else {
            $link = $label;
        }

        return strtr($template, ['{link}' => $link]);
    }
}
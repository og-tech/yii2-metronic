<?php
return [
    'components' => [
        'metronic' => [
            'class' => 'offgamers\metronic\Metronic',
            'sourcePath' => '@backend/theme/metronics/assets',
            'buildPath' => 'demo/demo11'
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [],
                    'depends' => [
                        'offgamers\metronic\bundles\MetronicAsset'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => false,
                'yii\bootstrap\BootstrapPluginAsset' => false,
            ],
        ],
    ],
    'view' => [
        'theme' => [
            'pathMap' => [
                '@mdm/admin/views' => '@backend/views'
            ],
        ],
    ],
];
